#+title: Patronage

A free software patronage and digitial store platform written with [[https://www.gnu.org/software/guile/][Guile Scheme]].

* Usage

Instructions forthcoming.

* License

This project is distributed under the [[file:LICENSE][AGPL 3.0]] license.
