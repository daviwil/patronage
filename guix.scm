(use-modules
  (guix git)
  (guix packages)
  (guix build-system gnu)
  (guix licenses)
  (gnu packages guile)
  (gnu packages guile-xyz))

(package
  (name "patronage")
  (version "0.1.dev")
  (source (git-checkout (url (dirname (current-filename)))))
  (build-system gnu-build-system)
  (inputs (list guile-3.0-latest artanis))
  (synopsis "A free software patronage and digitial store platform.")
  (description"A free software patronage and digitial store platform written with Guile Scheme.")
  (home-page "https://codeberg.org/daviwil/patronage/")
  (license agpl3+))
